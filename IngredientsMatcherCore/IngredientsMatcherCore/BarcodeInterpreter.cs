﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing;
using System.Drawing;
using System.IO;

namespace IngredientsMatcherCore
{
    class BarcodeInterpreter
    {
        public string GetBarcode(string filename)
        {
            IBarcodeReader reader = new BarcodeReader();
            if(File.Exists(filename) == false)
            {
                Console.WriteLine("The image does not exist");
                Console.ReadKey(true);
                return null;
            }

            Bitmap barcodeToScan = new Bitmap(filename);


            //string barcodeType = "";
            string barcodeContent = "";
            string badBarcode = null;

            var scannedBarcode = reader.Decode(barcodeToScan);
            if (scannedBarcode != null)
            {
                //barcodeType = scannedBarcode.BarcodeFormat.ToString(); /* *probably* don't need this */
                barcodeContent = scannedBarcode.Text;
                return barcodeContent;
            }
            else
            {
                Console.WriteLine("Failed reading barcode.");
                return badBarcode;
            }
        }
    }
}
