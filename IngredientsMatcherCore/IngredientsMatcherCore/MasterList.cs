﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IngredientsMatcherCore
{
    class MasterList
    {
        private List<string> masterFileList = new List<string>();
        private List<Ingredients> ingredientList = new List<Ingredients>();
        private const string MasterFileLocation = @"C:\IngredientsMatcher\MasterFile.txt";
        private const char MajorSeperatorCharacter = ':';
        private const char MinorSeperatorCharacter = ',';


        public string SelectBarcodeToScan()
        {
            List<string> barcodeFiles = new List<string>();
            string directory = @"C:\IngredientsMatcher\Barcodes\";
            try
            {
                barcodeFiles = Directory.GetFiles(directory).ToList();
            }
            catch(DirectoryNotFoundException)
            {
                Console.WriteLine("Directory not found");
                Console.ReadKey(true);
                Console.Clear();
                return null;
            }
            Console.WriteLine("Pick the image you want to interpret:");
            for (int loopIndex = 0; loopIndex < barcodeFiles.Count; loopIndex++)
            {
                Console.WriteLine((loopIndex + 1) + ". " + barcodeFiles[loopIndex]);
            }

            int userSelection = -1;

            try
            {
                userSelection = int.Parse(Console.ReadLine()) - 1;
            }
            catch (FormatException)
            {
                Console.WriteLine("That is not a number.");
                Console.ReadKey(true);
                Console.Clear();
                return null;
            }
            catch (OverflowException)
            {
                Console.WriteLine("The input is too big.");
                Console.ReadKey(true);
                Console.Clear();
                return null;
            }
            if (((userSelection + 1) < 1) || ((userSelection + 1) > barcodeFiles.Count))
            {
                Console.WriteLine("The input number is not in the selection");
                Console.ReadKey(true);
                Console.Clear();
                return null;
            }
            else
            {
                return barcodeFiles[userSelection];
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        public void ShowDish(string categoryName) /* Shows what ingredients a user needs for a dish */
        {
            Console.WriteLine("For " + categoryName + " you will need:");
            Ingredients categoryChecker = new Ingredients();
            int ingredientNumber = 1;
            double totalPrice = 0;
            int numberOfVegIngredients = 0;
            int numberOfIngredients = 0;


            for (int loopIndex = 0; loopIndex < ingredientList.Count; loopIndex++)
            {

                if (categoryChecker.ExistsInCategory(categoryName, ingredientList[loopIndex]))
                {

                    Console.WriteLine(ingredientNumber + ". " + ingredientList[loopIndex].Name + ". Price: "
                        + ingredientList[loopIndex].Price);
                    totalPrice = totalPrice + ingredientList[loopIndex].Price;
                    numberOfIngredients++;
                    ingredientNumber++;


                    if (categoryChecker.CheckIfTagExists(ingredientList[loopIndex], "veg")) /* To add more tags and their behavours, add more */
                    {                                                                       /* if else blocks here */
                        numberOfVegIngredients++;
                    }
                }
            }
            Console.WriteLine("\n\tTotal price: " + totalPrice);
            if (numberOfVegIngredients == numberOfIngredients)
            {
                Console.WriteLine("\n\n\tNOTE: This dish is a vegetarian dish.");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////

        public Ingredients GetProduct(string scannedBarcode) /* Finds product by barcode! */
        {
            bool foundIndex = false;
            int indexOfCorrectObject = -1;
            for (int currentIndexForThisLoop = 0; currentIndexForThisLoop < ingredientList.Count; currentIndexForThisLoop++)
            {
                if (string.Compare(ingredientList[currentIndexForThisLoop].Barcode, scannedBarcode) == 0)
                {
                    foundIndex = true;
                    indexOfCorrectObject = currentIndexForThisLoop;
                }
            }
            if (foundIndex == false)
            {
                return null;
            }
            else
            {
                return ingredientList[indexOfCorrectObject];
            }

        }

        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////

        public void ReadFromFile()
        {
            try
            {
                if (File.Exists(MasterFileLocation) == false) /* Checks if the master file exists, and if not, makes an empty one */
                {
                    File.Create(MasterFileLocation);
                    Console.WriteLine("The master list is empty.");
                    Console.ReadKey(true);
                    Environment.Exit(0);
                }
            }
            catch (DirectoryNotFoundException) /* If the master list directory does not exist, it creates it and starts over */
            {
                Directory.CreateDirectory(@"C:\IngredientsMatcher");
                ReadFromFile();
            }



            masterFileList = File.ReadAllLines(MasterFileLocation).ToList(); /* Reads from file and puts the contents in a list */

            if (masterFileList.Count == 0)
            {
                Console.WriteLine("The master list is empty.");
            }

            int numberOfEntries = masterFileList.Count();
            int currentIngredientIndex = -1;
            bool cleanFile;

            do /* This do-while loop basically makes sure the program doesn't give out syntax errors if there are */
            { /* empty lines at the end of the file */
                if (masterFileList[numberOfEntries - 1] == "")
                {
                    cleanFile = false;
                    masterFileList.RemoveAt(numberOfEntries - 1);
                    numberOfEntries = masterFileList.Count();
                }
                else
                {
                    cleanFile = true;
                }
            } while (cleanFile == false);


            try
            {
                for (currentIngredientIndex = 0; currentIngredientIndex < masterFileList.Count; currentIngredientIndex++)
                {
                    string[] tempIngredientArray = new string[3];
                    Ingredients ingredientObject = new Ingredients();
                    tempIngredientArray = masterFileList[currentIngredientIndex].Split(MajorSeperatorCharacter);
                    //
                    ingredientObject.Name = tempIngredientArray[0];
                    //
                    ingredientObject.Barcode = tempIngredientArray[1];
                    //
                    ingredientObject.Price = (double.Parse(tempIngredientArray[2])) / 100.0;

                    if (tempIngredientArray[3] != null)
                    {
                        ingredientObject.Tags = tempIngredientArray[3].Split(MinorSeperatorCharacter).ToList();
                    }


                    if (tempIngredientArray[4] != null)
                    {
                        ingredientObject.Categories = tempIngredientArray[4].Split(MinorSeperatorCharacter).ToList();
                    }

                    ingredientList.Add(ingredientObject);
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Syntax error in the master list! Line " + (currentIngredientIndex + 1)); /* The line indicator is */
                while (true) ;                                                                              /* actually useful */
            }
            catch (FormatException)
            {
                Console.WriteLine("Syntax error in the master list! Line " + (currentIngredientIndex + 1));
                while (true) ;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////


        public string CategorySelection(Ingredients categorySource) /* Data type might change */
        {                                                           /* Spoiler alert: it didn't */
            Console.WriteLine("Select the number of the dish you want to look at:");
            for (int indexForCurrentLoop = 0; indexForCurrentLoop < categorySource.Categories.Count; indexForCurrentLoop++)
            {
                Console.WriteLine((indexForCurrentLoop + 1) + ". " + categorySource.Categories[indexForCurrentLoop]);
            }
            int selectedCategory = -1;
            try
            {
                selectedCategory = int.Parse(Console.ReadLine()) - 1;
            }
            catch (FormatException)
            {
                Console.WriteLine("Bad input.");
                Console.ReadKey(true);
                Console.Clear();
                return null;
            }
            if ((selectedCategory >= categorySource.Categories.Count) || (selectedCategory < 0)) /* Checks to see if the input */
            {                                                                                     /* has value */
                Console.WriteLine("Selection not valid");
                Console.ReadKey(true);
                Console.Clear();
                return null;
            }
            else
            {
                return categorySource.Categories[selectedCategory];
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////

    }
}
