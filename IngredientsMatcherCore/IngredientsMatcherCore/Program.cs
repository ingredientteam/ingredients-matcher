﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientsMatcherCore
{
    class Program
    {
        static void Main(string[] args)
        {
            MasterList fileContent = new MasterList();


            fileContent.ReadFromFile();
            Ingredients selectedIngredient;
            BarcodeInterpreter barcode = new BarcodeInterpreter();
            string scannedBarcode = "";
            string barcodeFilename = "";
            do
            {
                barcodeFilename = fileContent.SelectBarcodeToScan();
            } while (barcodeFilename == null);

            do
            {
                scannedBarcode = barcode.GetBarcode(barcodeFilename); /* remember to make this variable, idiot */
                if (scannedBarcode == null)
                {
                    Console.ReadKey(true);
                    Console.Clear();
                }
            } while (scannedBarcode == null);

            Console.WriteLine("You scanned: " + scannedBarcode);
            //scannedBarcode = userInput.ScanIngredient();
            selectedIngredient = fileContent.GetProduct(scannedBarcode);
            if (selectedIngredient == null)
            {
                Console.WriteLine("Such a product does not exist");
                Console.ReadKey(true);
                Environment.Exit(0); /* Closes the program, remember to replace it with starting over, later */
            }

            string selectedCategory = "";
            do
            {
                selectedCategory = fileContent.CategorySelection(selectedIngredient);
            } while (selectedCategory == null);



            Console.Clear();
            fileContent.ShowDish(selectedCategory);
            Console.ReadKey(true);
        }
    }
}
