﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngredientsMatcherCore
{
    class Ingredients
    {
        public string Name { get; set; }
        public string Barcode { get; set; }
        public double Price { get; set; }
        public List<string> Tags = new List<string>();
        public List<string> Categories = new List<string>(); 

        public bool ExistsInCategory(string categoryName, Ingredients ingredientObject)
        {
            bool categoryExists = false;
            for (int loopIndex = 0; loopIndex < ingredientObject.Categories.Count(); loopIndex++)
            {
                if(ingredientObject.Categories[loopIndex] == categoryName)
                {
                    categoryExists = true;
                }
            }
            return categoryExists;
        }


        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////

        public bool CheckIfTagExists(Ingredients product, string tag)
        {
            if (product.Tags.Contains(tag))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
