# **Ingredients Matcher�**


### What is Ingredients Matcher�?

 Ingredients Matcher� is a system for shoppers to be able to quickly look
 at what they can cook with a foodstuff product and what ingredients they would
 need, on the fly.

### How does it work?
 
 In the finished version, we would like for the program to be able to scan
 a barcode through a webcam, but currently we only have the interpreter, which
 requires you to have an image called TestingBarcode.png (TestingBarcode.bmp
 was also tested) with a picture of a barcode.

### Where does it store the information?

 Ingredients Matcher� has a special file aptly called "MasterFile.txt" in the 
 C:\IngredientsMatcher\ directory which contains all the information required 
 for the program to function.

### What does the file look like

 The syntax is:

 Name of product:barcodenumber:tags,product,has:pricein�re:dish,ingredient belongs,to

### What if I mess up the syntax?

 Something among the lines of "Syntax error in line 16" will show up when you try
 running the program.

# **What needs to be done**
### Webcam capture

 Who knows how this works. Magnets?!
 No, ZXing.NET *has* a code example, use it to figure it out.
 https://zxingnet.codeplex.com/SourceControl/latest#trunk/Clients/WindowsFormsDemo/WebCam.cs

### Making the program more modular
 We were kind of lazy with the design, but after Lene's domain model ~~lecture~~ destruction, it apparently became clear that the way we handle the masterfile (specifically storing the data from it into objects) needs to be more modular.

### What the hell does that mean?
 Separate Dish object that has two properties: a name and a list of ingredients. This way, to populate a dish list from a ingredient name, you have to look through every dish's list of ingredients and show the ones which contain that member.
